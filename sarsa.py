import numpy
from random import randint, random


def world(x, y, num_holes, num_arrows):
    world_map = numpy.zeros((x, y))
    j = 0
    while j < num_arrows:
        # passing
        j += 1
        continue
        ###########
        x_arrows = randint(0, x-1)
        y_arrows = randint(0, y-1)
        if world_map[x_arrows, y_arrows] == 0:
            world_map[x_arrows, y_arrows] = 2
            if x_arrows-1 >= 0:
                world_map[x_arrows-1, y_arrows] = 2.5
            if x_arrows+1 < x-1:
                world_map[x_arrows+1, y_arrows] = 2.5
            if y_arrows-1 >= 0:
                world_map[x_arrows, y_arrows-1] = 2.5
            if y_arrows+1 < y-1:
                world_map[x_arrows, y_arrows+1] = 2.5
        else:
            continue
        j += 1
    i = 0
    while i < num_holes:
        x_hole = randint(0, x-1)
        y_hole = randint(0, y-1)
        if world_map[x_hole, y_hole] == 0:
            world_map[x_hole, y_hole] = -30
            if x_hole-1 >= 0:
                world_map[x_hole-1, y_hole] = -10
            if x_hole+1 < x-1:
                world_map[x_hole+1, y_hole] = -10
            if y_hole-1 >= 0:
                world_map[x_hole, y_hole-1] = -10
            if y_hole+1 < y-1:
                world_map[x_hole, y_hole+1] = -10
        else:
            continue
        i += 1
    x_wumpus = randint(0, x-1)
    y_wumpus = randint(0, y-1)
    if world_map[x_wumpus, y_wumpus] == 0:
        world_map[x_wumpus, y_wumpus] = -50
        if x_wumpus-1 >= 0:
            world_map[x_wumpus-1, y_wumpus] = -20
        if x_wumpus+1 < x-1:
            world_map[x_wumpus+1, y_wumpus] = -20
        if y_wumpus-1 >= 0:
            world_map[x_wumpus, y_wumpus-1] = -20
        if y_wumpus+1 < y-1:
            world_map[x_wumpus, y_wumpus+1] = -20
    else:
        print("failed...\n retrying...")
        return world(x, y, num_holes, num_arrows)
    world_map[0, 0] = 5
    # print(world_map)
    return world_map


def take_action(px, py):
    pr = None
    pl = None
    pt = None
    pd = None
    # right
    if px + 1 < 10:
        pr = px + 1
    # top
    if py - 1 >= 0:
        pt = py - 1
    # left
    if px - 1 >= 0:
        pl = px - 1
    # down
    if py + 1 < 10:
        pd = py + 1

    action_p = random()
    action = None
    best_action_value = 0

    if action_p < explore_rate:
        # random action
        _ = randint(0, 3)
        if _ == 0 and pr:
            action = [pr, py]
            best_action_value = m[pr, py]
        if _ == 1 and pt:
            action = [px, pt]
            best_action_value = m[px, pt]
        if _ == 2 and pl:
            action = [pl, py]
            best_action_value = m[pl, py]
        if _ == 3 and pd:
            action = [px, pd]
            best_action_value = m[px, pd]
    if action is None:
        # best action
        if pt:
            if best_action_value < m[px, pt] or not action:
                best_action_value = m[px, pt]
                action = [px, pt]
        if pl:
            if best_action_value < m[pl, py] or not action:
                best_action_value = m[pl, py]
                action = [pl, py]
        if pr:
            if best_action_value < m[pr, py] or not action:
                best_action_value = m[pr, py]
                action = [pr, py]
        if pd:
            if best_action_value < m[px, pd] or not action:
                best_action_value = m[px, pd]
                action = [px, pd]

    return action, best_action_value


m = world(10, 10, 5, 3)
print(m)

explore_rate = .2
lambda_rate = 0.2
alpha_rate = .2

policy = numpy.zeros((10, 10))
v = {}
returns = {}
for i in range(5):
    print(i)
    px = 9
    py = 9
    episodes = []
    while True:
        action, r = take_action(px, py)
        if m[action[0], action[1]] == 5 or m[action[0], action[1]] == -30 or m[action[0], action[1]] == -50:
            break
        m[action[0], action[1]] += alpha_rate * (r + lambda_rate * m[action[0], action[1]] - m[px, py])
        px, py = action[0], action[1]


print(m)
